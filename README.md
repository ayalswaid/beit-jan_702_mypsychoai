# **PsychoAI**


What is PsychoAI?
It is a Magshimim project, created by: Saleem kheralden, Ayal swead (12th grade) with the guide of the mentors Kfir and Maor.<br>
PsychoAI is a personal psychologist that helps you to get through hard days and keep you company in the good days.
you can consider it your AI friend
Don't forget that this version is still BETA version so the AI might give false results, if you leave a feed back to improve the AI we will be grateful.
First you need to login or sign up if you're not signed in, when you do so an account will register in the server and you can start you conversations with the AI
The input it expects for is a short message about how was your day, what do feel today. the AI will try to analysis the data given to it to return a good, decent answer
it will return ether suggestion for a song, game, activity to do, article to read or a book

# Recommended development environment
    - PyCharm (Server, Machine learning)
    - Flutter (App)
# Built With
    
    - Python - frameworks: flask(Server), tensorflow & keras(Machine learning)
    - HTML/CSS/JS/BootStrap (Web Frontend)
    - Dart - framework: flutter(Mobile Appliciation)

# Build with
**Server Side**: run python file: PsychoAI/Flask/Web.py
    `python PsychoAI/Flask/Web.py`.
**Client Side/application**: our app work with flutter SDK, so to build an .apk file enter the following command in the cmd in PsychoAI_app folder:
`flutter build`
to build an android app bundle (.aab) file:
`flutter build appbundle`
and find the build file in `PsychoAI_app/build/outputs/apk` or `PsychoAI_app/build/outputs/bundle/release/`
# Installing
    git clone https://gitlab.com/ayalswaid/beit-jan_702_mypsychoai.git

# Requirements

    file --> requirements.txt
    
    
# References

*  [Nearly everything you have to know about sentiment analysis](https://monkeylearn.com/sentiment-analysis/)
*  [Deep learning classification techniques](https://medium.com/datadriveninvestor/deep-learning-techniques-for-text-classification-9392ca9492c7)
*  [Emotional chatting machine](https://arxiv.org/pdf/1704.01074.pdf)


© 2019 PsychoAI, Inc.
    
