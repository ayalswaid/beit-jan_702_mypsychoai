from __future__ import absolute_import, division, print_function, unicode_literals
from flask import Flask, request, render_template, send_file, redirect, session, g, make_response
import os
from SQL_Wrapper.sql import INSERT, UPDATE, DELETE, SELECT
from werkzeug.security import generate_password_hash, check_password_hash
from flask_socketio import SocketIO
from datetime import datetime
import random
import pandas as pd
import subprocess

from ML.emotion_rec_alt import model
from ML.text_generator import motivation_model


bad_emotions = ['sadness', 'fear', 'anger']
good_emotions = ['happiness', 'surprise', 'love']

# Question bank used to know the use's interests and hobbies. the data gathered will be saved in a dataset in a database
qes_bank = ["What do you do in your free time?", "What's your favorite color?",
            "What do you do when you're feeling low?"]

answer_bank = ["""<form method="post"> 
                <input type="checkbox" name="swimming"> 
                <span>swimming</span> 
                <br /> 
                <input type="checkbox" name="reading"> 
                <span>reading</span> 
                <br /> 
                <input type="checkbox" name="cooking"> 
                <span>cooking</span> 
                <br /> 
                <input type="checkbox" name="playingvg"> 
                <span>playing video games</span> 
                <br /> 
                <input type="checkbox" name="eating"> 
                <span>eating</span>
                <br />
                <span>other</span>
                <input type="text" name="other">
                <br />
                <button type="submit" class="send-btn">ok</button>
                </form>
                """, """<form method="post"> 
                <input type="checkbox" name="Blue"> 
                <span>Blue</span> 
                <br /> 
                <input type="checkbox" name="Red"> 
                <span>Red</span> 
                <br /> 
                <input type="checkbox" name="White"> 
                <span>White</span> 
                <br /> 
                <input type="checkbox" name="Black"> 
                <span>Black</span> 
                <br /> 
                <input type="checkbox" name="Yellow"> 
                <span>Yellow</span>
                <br />
                <span>other</span>
                <input type="text" name="other">
                <br />
                <button type="submit" class="send-btn">ok</button>
                </form>""", """<form method="post"> 
                <input type="checkbox" name="TalkDad"> 
                <span>Talk to my dad</span> 
                <br /> 
                <input type="checkbox" name="TalkMom"> 
                <span>Talk to my mom</span> 
                <br /> 
                <input type="checkbox" name="Write"> 
                <span>Write about it</span> 
                <br /> 
                <input type="checkbox" name="Sleep"> 
                <span>Sleep</span> 
                <br /> 
                <input type="checkbox" name="TalkFriend"> 
                <span>Talk to a friend</span>
                <br />
                <span>other</span>
                <input type="text" name="other">
                <br />
                <button type="submit" class="send-btn">ok</button>
                </form>"""]

# this file is to get the quotes and display one of the quotes will be picked as "Quote of the day" for the user.
file = open("C:\\Users\\PC\\Desktop\\PsychoAI\\ML\\motivation_datasets\\quote.data", "r", encoding="utf8")
lines = file.readlines()

day = datetime.now().date()
ran = random.randint(0, len(lines))

# this dataset is to return responses to user according to his input which will be processed in the ML directory
data = pd.read_csv(r"C:\\Users\\PC\\Desktop\\PsychoAI\\Data\\quotes-500k\\quotes.csv")

quotes = data["quote"].values.tolist()
authors = data["author"].values.tolist()
category = data["category"].values.tolist()

app = Flask(__name__)
# (id, firstname, lastname, email, username, password (hashed), admin)

app.secret_key = os.urandom(24)
socketio = SocketIO(app)

publicIp = "82.81.32.217"
ip = subprocess.check_output("ipconfig").__str__().split("Wireless LAN adapter Wi-Fi")[1].__str__().split("IPv4 Address")[1].__str__().split(":")[1].__str__().split("\\r")[0].__str__().strip()
port = 5001
domainName = "psychoai.onthewifi.com"
# connectionStr = "http://{}".format(domainName)
connectionStr = "https://{}:{}".format(publicIp, port)
# current connected users in the system
online_connected_users = 0
usersInHome = list()

# percentage
register_percentage_base = 0
register_percentage_count = 0
register_percentage = 0

download_percentage_base = 0
download_percentage_count = 0
download_percentage = 0


# before request ->
@app.before_request
def before_request():
    if not 'tools' in session:
        session['tools'] = {"QesIndex": 0,
                            "QuoteIndex": 0,
                            "quote": "",
                            "author": "",
                            "emotion": "",
                            "response": ""}
    
    # this block of code will check if a day has passed to update the "Quote of the day"
    global day
    global ran
    global register_percentage_base, download_percentage, download_percentage_count, register_percentage, register_percentage_count, download_percentage_base
    if datetime.now().date() != day:
        ds = SELECT("SELECT users, downloads FROM admin_tbl")
        for row in ds:
            register_percentage_base = row[0]
            download_percentage_base = row[1]
            break
        download_percentage = 0
        download_percentage_count = 0
        register_percentage = 0
        register_percentage_count = 0
        day = datetime.now().date()
        ran = random.randint(0, len(lines))

    g.user = None
    if 'user' in session:
        g.user = session['user']

# Error handler page
@app.errorhandler(404)
def error_404(e):
    return render_template('404.html')

# if the systems detects an attempt for sql injection it will redirect the user to this page to prevent the injection
@app.route('/SQL_Injection')
def hacker_detection():
    try:
        return "SQL injection detected by:" + session['user']['username']
    except:
        return redirect('/', code=302)

# main page ->
@app.route('/')
def main_page():
    if not 'tools' in session:
        session['tools'] = {"QesIndex": 0,
                            "QuoteIndex": 0,
                            "quote": "",
                            "author": "",
                            "emotion": "",
                            "response": ""}

    quoteOfTheDay = lines[ran].split('\t')[1]
    authorOfTheDay = lines[ran].split('\t')[0]

    return render_template('MainPage.html', quote_of_the_day=quoteOfTheDay, authorOfTheDay=authorOfTheDay)

# download the app
@app.route('/download')
def download_app():
    qry = "SELECT downloads FROM admin_tbl"
    ds = SELECT(qry)
    for row in ds:
        downloads = row[0]

        global download_percentage_count
        global download_percentage
        global download_percentage_base
        download_percentage_count += 1
        download_percentage = (download_percentage_count / download_percentage_base) * 100

        newDownloadQry = "UPDATE admin_tbl SET downloads={} WHERE id=1".format(downloads + 1)
        UPDATE(newDownloadQry, 'admin_tbl')
        socketio.emit('download_server', {'number': downloads + 1, 'percentage': download_percentage}, broadcast=True)
        break
    return redirect('https://drive.google.com/file/d/1OHlWlpU-owhCFr_KWUPGh_uIViWINsow/view?usp=sharing', code=302)

# login page ->
@app.route('/Login', methods=['POST', 'GET'])
@app.route('/Login.html', methods=['POST', 'GET'])
def create_login_page():
    if request.method == 'POST':
        # take the username and the password from the user webpage.
        # and check if this username exists and if so if the password is his
        username = request.form['username-textbox']
        password = request.form['password-textbox']
        checkbox = 'off'

        try:
            # this checkbox used to know wither the user wants the system to remember his
            # username and password to skip the login stage using cookies that'll be stored in the webpage
            checkbox = request.form['checkbox']
        except:
            pass

        # if the username or the password has this -- char it might be an attempt for sql injection
        if username.__contains__('--'):
            username = username.replace('--', '')
        if password.__contains__('--'):
            password = password.replace('--', '')


        # this login function checks if this username and password belongs to an existing user
        # and if so returns true else returns false
        if login(username, password):
            # if the checkbox is checked 'on' that means the user wants the system
            # to save his username and password to skip the login stage in the future
            # so this block of code creates a cookie  to save it in the users webpage
            # which has the a lifetime of 10 days from it's creation
            # if the checkbox is off the login stage will continue without creating a cookie
            if checkbox == 'on':
                resp = make_response(redirect('home'))
                resp.set_cookie('user',
                                value=username+":"+password,
                                max_age=60 * 60 * 24 * 10,
                                path='/',
                                domain=None,
                                secure=False,
                                httponly=False
                                )
                return resp
                #redirect('home', code=302)
            else:
                return redirect('home', code=302)
        else:
            return render_template('Login.html', error='username or password is not correct')

    # this block of code checks if there's a cookie in the storage and if so
    # it extracts the data from it to login automatically
    try:
        cookie = request.cookies
        user = cookie.get("user")
        username = user.split(':')[0]
        password = user.split(':')[1]

        if login(username, password):
            return redirect('home')
    except:
        return render_template('Login.html')


def login(username, password):
    loginQry = "SELECT * FROM Users_Data_tbl WHERE Username='" + username + "'"
    cursor = SELECT(loginQry)
    try:
        for data in cursor:
            if check_password_hash(data[5], password) and data[4] == username:
                user_data = {"id": data[0],
                             "firstname": data[1],
                             "lastname": data[2],
                             "email": data[3],
                             "username": data[4],
                             "password": data[5],
                             "hobbies": data[6],
                             "fav_color": data[7],
                             "when_low": data[8],
                             "admin": data[9]}
                # session["KEY"] = "VALUE"
                session['user'] = user_data
                return True
    except:
        pass
    return False

# sign up page ->
@app.route('/SignUp', methods=['POST', 'GET'])
def signup():
    tbl = "Users_Data_tbl"
    # this flag is used to indicates wither the user exists in the database
    flag = True
    if request.method == 'POST':
        firstname = request.form['firstname-textbox']
        lastname = request.form['lastname-textbox']
        email = request.form['email-textbox']
        username = request.form['username-textbox']

        # if there's an appearance of this char -- it might be an attempt for sql injection
        if username.__contains__('--'):
            username = username.replace('--', '')

        cursor = SELECT("SELECT * FROM " + tbl + " WHERE username='" + username + "'")
        for elem in cursor:
            flag = False
        password = request.form['password-textbox']
        ver_password = request.form['ver-password-textbox']

        if password.__contains__('--'):
            password = password.replace('--', '')

        if ver_password.__contains__('--'):
            ver_password = ver_password.replace('--', '')

        # if there's no user with this information in the database
        # and the password and the verification password matches the system will add this user to it's database
        if flag and password == ver_password:
            hashed_password = generate_password_hash(password, method='sha256')
            password = hashed_password
            qry = "INSERT INTO Users_Data_tbl (FirstName, LastName, Email, Username, Password, IsAdmin) "
            qry += "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', 0)"
            qry = qry.format(firstname, lastname, email, username, password)
            if INSERT(qry, tbl):
                ds = SELECT("SELECT users FROM admin_tbl")
                for row in ds:
                    users = row[0]
                    global  register_percentage_count
                    global register_percentage
                    global register_percentage_base
                    register_percentage_count += 1
                    register_percentage = (register_percentage_count / register_percentage_base) * 100
                    insertUserToAdminTblQry = "UPDATE admin_tbl SET users={} WHERE id=1".format(users + 1)
                    UPDATE(insertUserToAdminTblQry, 'admin_tbl')
                    # true means you can insert, false means you need to update because there's already a row with this date
                    flag = True
                    selectQry = "SELECT users, _date FROM admin_tbl WHERE _date='{}'"\
                        .format(datetime.now().strftime('%Y-%m-%d').__str__())
                    ds = SELECT(selectQry)
                    usersToUpdate = 0
                    for row in ds:
                        flag = False
                        usersToUpdate = int(row[0])
                        break

                    if flag:
                        insertRegisterDate = "INSERT INTO admin_tbl (users, _date) VALUES (1, '{}')".format(
                            datetime.now().strftime('%Y-%m-%d'))
                        INSERT(insertRegisterDate, 'admin_tbl')
                    else:
                        insertRegisterDate = "UPDATE admin_tbl SET users={} WHERE _date='{}'".format(usersToUpdate + 1,
                            datetime.now().strftime('%Y-%m-%d'))
                        UPDATE(insertRegisterDate, 'admin_tbl')

                    try:
                        socketio.emit('register_server', {'number': users + 1, 'percentage': register_percentage},
                                      broadcast=True)
                    except:
                        pass
                    break
                return redirect('Login', code=302)
        elif password != ver_password:
            return render_template('SignUp.html', error="password and Verify password doesn't match")
        else:
            return render_template('SignUp.html', error='username already in use')
    elif request.method == 'GET':
        return render_template('SignUp.html')


# Variables for the home page

PostHandler = [['swimming', 'reading', 'cooking', 'playingvg', 'eating', 'other'],
             ['Blue', 'Red', 'White', 'Black', 'Yellow', 'other'],
             ['TalkDad', 'TalkMom', 'Write', 'Sleep', 'TalkFriend', 'other']]


# home page ->
@app.route('/home', methods=['GET', 'POST'])
def home_page():
    try:
        quote = session['tools']['quote']
        author = session['tools']['author']
        emotion = session['tools']['emotion']
    except:
        quote = ""
        author = ""
        emotion = ""

    try:
        if not session['user']['id'] in usersInHome:
            global online_connected_users
            online_connected_users += 1
            socketio.emit('onlineUsers_server', {'number': online_connected_users}, broadcast=True)
            socketio.emit('newLogin_server', {'user': session['user']})
            INSERT('INSERT INTO online_tbl (id) VALUES ({})'.format(session['user']['id']), 'online_tbl')
            usersInHome.append(session['user']['id'])
    except:
        return redirect('/Login')

    tbl = 'user_msg_tbl'
    arr = get_history()
    response = ""
    # this is the list of the users answer for the question from the "qes_bank"
    usersAnswerForQesText = ""
    UpdateUserDataQry = ""
    if request.method == 'POST':
        try:
            if session['tools']['QesIndex'] > -1:
                for elem in PostHandler[session['tools']['QesIndex']]:
                    try:
                        if request.form[elem].__str__() == 'on':
                            usersAnswerForQesText = usersAnswerForQesText + ":" + elem
                        if elem == 'other' and request.form[elem].__str__() != '':
                            usersAnswerForQesText += ":" + request.form[elem].__str__()
                    except:
                        pass
            try:
                if session['tools']['QesIndex'] == 0:
                    UpdateUserDataQry = "UPDATE Users_Data_tbl SET hobbies='{}' WHERE ID={}".format(
                        usersAnswerForQesText, session['user']['id'])
                    session['user']['hobbies'] = usersAnswerForQesText
                elif session['tools']['QesIndex'] == 1:
                    UpdateUserDataQry = "UPDATE Users_Data_tbl SET fav_color='{}' WHERE ID={}".format(
                        usersAnswerForQesText, session['user']['id'])
                    session['user']['fav_color'] = usersAnswerForQesText
                elif session['tools']['QesIndex'] == 2:
                    UpdateUserDataQry = "UPDATE Users_Data_tbl SET when_low='{}' WHERE ID={}".format(
                        usersAnswerForQesText, session['user']['id'])
                    session['user']['when_low'] = usersAnswerForQesText
                if UpdateUserDataQry != '':
                    session.modified = True
                    UPDATE(UpdateUserDataQry, 'Users_Data_tbl')
            except:
                pass

            try:
                msg = request.form['message-bar']
                if msg.__contains__('--'):
                    # return redirect('SQL_Injection', code=302)
                    msg = msg.replace('--', '')

                # the system takes every message that has been sent by the client or the server
                # and stores it in the database for future use like showing the history of the chat
                user_id = session['user']['id']
                qry = "INSERT INTO " + tbl + " (id, msg, msg_author, msg_date)"
                qry += "VALUES ({0}, '{1}', 'client', '{2}')"
                qry = qry.format(user_id, msg.replace("'", "''"), datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                INSERT(qry, tbl)

                # this line of code calls the model to figure out the feeling in the client's message
                emotion = model.guess(msg.__str__())
                quote = motivation_model.generate(emotion)

                INSERT("INSERT INTO mood_list (user_id, mood, date) VALUES ({}, '{}', '{}')".format(user_id, emotion,
                                                                                                    datetime.now().strftime(
                                                                                                        '%Y-%m-%d %H:%M:%S')),
                       "mood_list")
                # this search function searches the quote dataset
                # to gather every quote that is related to the detected emotion

                # lst = search(emotion)
                # quote = lst[0]
                author = ""

                global bad_emotions, good_emotions
                if bad_emotions.__contains__(emotion):
                    ds = SELECT("SELECT when_low FROM Users_Data_tbl WHERE ID={}".format(session['user']['id']))
                    activites = list()
                    for row in ds:
                        lst = row[0].__str__().split(':')[1:]
                        for elem in lst:
                            activites.append(elem)

                    try:
                        response = "It's bad seeing you feeling this way, Why don't you do one of your favorite activities?\n" \
                                   "Like {}".format(activites[random.randint(0, len(activites) - 1)])
                    except:
                        response = "It's bad seeing you feeling this way, Why don't you do one of your favorite activities?\n"

                else:
                    ds = SELECT("SELECT hobbies FROM Users_Data_tbl WHERE ID={}".format(session['user']['id']))
                    activites = list()
                    for row in ds:
                        lst = row[0].__str__().split(':')[1:]
                        for elem in lst:
                            activites.append(elem)

                    try:
                        response = "I'm happy for you, Why don't you do one of your favorite activities?\n" \
                               "Like {}".format(activites[random.randint(0, len(activites) - 1)])
                    except:
                        response = "I'm happy for you, Why don't you do one of your favorite activities?\n"

                session['tools']['emotion'] = emotion
                # session.modified = True
                session['tools']['quote'] = quote
                # session.modified = True
                session['tools']['author'] = ""
                # session.modified = True
                session['tools']['QuoteIndex'] = 0
                session['tools']['response'] = response
                session.modified = True

                # print(session['tools'])

                qry = "INSERT INTO " + tbl + " (id, msg, msg_author, msg_date)"
                qry += "VALUES ({0}, '{1}', 'server', '{2}')"
                qry = qry.format(user_id, quote[session['tools']['QuoteIndex']].replace("'", ""),
                                 datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

                INSERT(qry, tbl)

                arr = get_history()
                for i, elem in enumerate(arr, 0):
                    if elem.__contains__("''"):
                        arr[i] = elem[0].replace("''", "'")

                return redirect('/home', code=302)
            except Exception as e:
                print(e)
                return redirect('/home', code=302)

        except Exception as e:
            print(e)
            if e.__str__().__contains__('tools'):
                session['tools'] = {"QesIndex": 0,
                                    "QuoteIndex": 0,
                                    "quote": "",
                                    "author": "",
                                    "emotion": "",
                                    "response": ""}
            return redirect('Login', code=302)
    else:
        try:
            if g.user:

                UseQestionDataSet = SELECT("SELECT hobbies, fav_color, when_low FROM Users_Data_tbl WHERE ID={}".format(session['user']['id']))
                unansweredQes = list()
                for row in UseQestionDataSet:
                    for i, col in enumerate(row, 0):
                        if col == '' or col == None or col == 'NULL':
                            unansweredQes.append(i)
                if len(unansweredQes) != 0:
                    unansweredQesIndex = random.randint(0, len(unansweredQes) - 1)
                    qes = unansweredQes[unansweredQesIndex]
                else:
                    qes = -1

                session['tools']['QesIndex'] = qes
                session.modified = True
                if qes != -1:
                    qes = qes_bank[session['tools']['QesIndex']]
                    ans = answer_bank[session['tools']['QesIndex']]
                else:
                    qes = "Write something down below for MIA to know how you feel!"
                    ans = ""

                return render_template('Home.html', response=session['tools']['response'], quote=quote, author=author, emotion=emotion, qes=qes, answer=ans, firstname=session['user']['firstname'], admin=admin_feature(), history=arr, connectionStr=connectionStr)
            else:
                return redirect('Login', code=302)
        except Exception as e:
            if e.__str__().__contains__('tools'):
                session['tools'] = {"QesIndex": 0,
                                    "QuoteIndex": 0,
                                    "quote": "",
                                    "author": "",
                                    "emotion": "",
                                    "response": ""}
            return redirect('Login', code=302)


def search(q):
    lst1 = list()
    lst2 = list()
    lst = list()
    for i, elem in enumerate(category, 0):
        try:
            if elem.__contains__(q):
                lst1.append(quotes[i])
                lst2.append(authors[i])
        except:
            pass

    lst.append(lst1)
    lst.append(lst2)
    return lst


def admin_feature():
    if session['user']['admin']:
        return '<li id="admin-dash-li"><a href="AdminDashboard">User dashboard</a></li>'
    return ''


def get_history():
    try:
        arr = []
        cursor = SELECT('SELECT * FROM user_msg_tbl WHERE id={0} ORDER BY msg_date ASC'.format(session['user']['id']))
        for elem in cursor:
            arr.append([elem[1], elem[2], elem[3].split('.')[0]])
        return arr
    except:
        return None

# profile page ->
@app.route('/home/profile', methods=['POST', 'GET'])
def profile_page():
    change = request.args.get("passwordChange")
    inputText = None
    if change == "true":
        inputText = "<form method='post'><input type='password' class='message-input' name='newpass' placeholder='Enter new password'><br/>" \
                    "<input type='password' class='message-input' name='vernewpass' placeholder='Renter new password'><br/>" \
                    "<a href='/home/profile?passwordChange=false'><button type='submit' class='send-btn'>Save</button></a></from>"

    try:
        if request.method == 'POST':
            password = request.form['newpass']
            verpassword = request.form['vernewpass']
            if password == verpassword:
                hashed_password = generate_password_hash(password, method='sha256')
                updatePass = "UPDATE Users_Data_tbl SET Password='{}' WHERE ID={}".format(hashed_password,
                                                                                          session['user']['id'])
                UPDATE(updatePass, 'Users_Data_tbl')
            inputText = None
    except:
        return redirect('/Login', code=302)

    try:
        username = session['user']['username']
        lastname = session['user']['lastname']
        firstname = session['user']['firstname']
        hobbiesStr = session['user']['hobbies']
        hobbies = list()
        if hobbiesStr != '':
            for elem in hobbiesStr.split(':')[1:]:
                hobbies.append(elem)

        email = session['user']['email']
        return render_template('profile.html', inputText=inputText, username=username, lastname=lastname, firstname=firstname, email=email, hobbies=hobbies)
    except:
        return redirect('/Login', code=302)

# stats page ->
@app.route('/home/stats')
def stats_page():
    try:
        if g.user:
            qry = 'SELECT mood, date FROM mood_list WHERE user_id={}'.format(session['user']['id'])
            ds = SELECT(qry)
            mood_data = list()
            for row in ds:
                temparr = list()
                for col in row:
                    temparr.append(col.__str__())
                mood_data.append(temparr)
            mood_labels = ["sadness", "happiness", "love", "surprise", "anger", "fear"]
            stats_data = [0, 0, 0, 0, 0, 0]
            for row in mood_data:
                stats_data[mood_labels.index(row[0])] += 1

            return render_template('stats.html', stats_data=stats_data)
        else:
            return redirect('/Login', code=302)
    except Exception as e:
        return redirect('/Login', code=302)

# history page ->
@app.route('/home/history')
def history():
    try:
        return render_template('history.html', history=get_history())
    except:
        return redirect('/Login', code=302)


# admin dash ->
@app.route('/AdminDashboard')
def admin_dash():
    try:
        if session['user']['admin']:
            ds = SELECT("SELECT users, downloads FROM admin_tbl WHERE id=1")
            admin_data = list()
            for row in ds:
                temp = list()
                for col in row:
                    temp.append(col)
                admin_data.append(temp)

            dataForTheChart = SELECT("SELECT users, _date FROM admin_tbl WHERE _date>'2002-08-05'")
            chartDataList = list()
            chartDataLabel = list()

            for row in dataForTheChart:
                chartDataList.append(row[0])
                chartDataLabel.append(row[1].__str__())

            global register_percentage, download_percentage

            return render_template('AdminDashBoard.html', connectionStr=connectionStr,
                                   chartDataLabel=chartDataLabel,
                                   chartDataList=chartDataList,
                                   download_percentage=download_percentage,
                                   register_percentage=register_percentage,
                                   admin_data=admin_data, online=online_connected_users)
        else:
            return render_template('404.html')
    except:
        return render_template('404.html')

# Admin dash extension ->
@app.route('/AdminDashboard/<state>')
def nav_state(state):
    try:
        if session['user']['admin']:
            if state == "online":
                return online_users()
            elif state == "users":
                requestedUser = request.args.get('user')
                if requestedUser == None:
                    return users("all")
                else:
                    return users(requestedUser)
        else:
            return render_template('404.html')
    except:
        return render_template('404.html')


def users(arg):
    flag = True
    if arg == "all":
        cursor = SELECT("SELECT ID, FirstName, LastName, Email, Username FROM Users_Data_tbl")
    else:
        flag = False
        cursor = SELECT("SELECT * FROM Users_Data_tbl WHERE id={}".format(arg))
    users_data = list()
    for data in cursor:
        temparr = list()
        for element in data:
            temparr.append(element)
        users_data.append(temparr)
    return render_template('users.html', users_data=users_data, all=flag.__str__())


def online_users():
    try:
        admin = session['user']['admin']
        if admin is True:
            online_cursor = SELECT('SELECT * FROM online_tbl')
            online_arr_id = []
            for user in online_cursor:
                online_arr_id.append(user[0])

            online_users_data = list()
            for id in online_arr_id:
                ds = SELECT('SELECT * FROM Users_Data_tbl WHERE ID={}'.format(id))
                for row in ds:
                    tempArray = list()
                    for i in range(5):
                        tempArray.append(row[i])
                    online_users_data.append(tempArray)


            return render_template('OnlineDash.html', connectionStr=connectionStr, online_users=online_users_data)
        else:
            return render_template('404.html')
    except:
        return render_template('404.html')


# chat rooms
rooms = {}

# chat rooms section
@socketio.on('newUserInRoom_client')
def new_user_handler(arg):
    socketio.emit('reciveMsg_server', arg)


@socketio.on('newMsg_client')
def new_msg_handler(arg):
    arg['time'] = datetime.now().strftime('%H:%M')
    socketio.emit('reciveMsg_server', arg)


@socketio.on('userLeftRoom_client')
def user_left_handler(arg):
    for elem in rooms:
        if rooms[elem].__contains__(arg['userid']):
            rooms[elem].remove(arg['userid'])
    socketio.emit('userLeftRoom_server', {'username': arg['username']})

# end chat rooms section
@app.route('/home/chatroomsLobby', methods=['POST', 'GET'])
def chat_rooms_lobby():
    if not 'user' in session:
        return redirect('/Login')

    room = request.args.get('room')

    if room != None:
        hashedRoom = redirect_to_room(room)
        return redirect('/home/chatroom/' + hashedRoom)

    if request.method == 'POST':
        room = request.form['roomname']
        hashedRoom = redirect_to_room(room)
        return redirect('/home/chatroom/' + hashedRoom)



    str = """
    <div class="col-md-4">
        <div class="info-box bg-info">
            <div class="online-box-content">
                <div class="sub-div">
                    <span name="roomname">{}</span>
                </div>
                
                <div class="sub-div">
                    <label id="online-count-text">Online users: </label>
                    <label id="online-count">{}</label>
                </div>
                
                <a href="/home/chatroomsLobby?room={}">
                    <div class="sub-div">
                        <button class="send-btn" style="border-radius: 25px;">
                            <span>Join</span>
                        </button>
                    </div>
                </a>
            </div>
        </div>
    </div>"""

    rows = list()
    cols = list()
    for i, elem in enumerate(rooms, 0):
        if i % 3 == 0 and i != 0:
            rows.append(cols)
            cols = list()
        connected = len(rooms[elem])
        cols.append(str.format(elem, connected, elem))
        if i == len(rooms) - 1:
            rows.append(cols)

    return render_template('chatroomsLobby.html', connectionStr=connectionStr, fisrtname=session['user']['firstname'], rows=rows)


def redirect_to_room(room):
    hashedRoom = generate_password_hash(room, 'sha256')
    session['userChat'] = {'id': session['user']['id'], 'username': session['user']['firstname'], 'room': room}

    try:
        if rooms[room].index(session['user']['id']) >= 0:
            pass
    except:
        if rooms.get(room) != None:
            if len(rooms[room]) > 0:
                rooms[room].append(session['user']['id'])
            else:
                rooms[room] = [session['user']['id']]
        else:
            rooms[room] = [session['user']['id']]
    return hashedRoom


@app.route('/home/chatroom/<roomname>')
def chatroom(roomname):
    if not 'user' in session:
        return redirect('/Login')
    return render_template('chatroom.html', roomname=session['userChat']['room'], user=session['userChat'], connectionStr=connectionStr)

# logout page ->
@app.route('/Logout', methods=['POST', 'GET'])
def logout():
    try:
        global online_connected_users
        if online_connected_users > 0:
            usersInHome.remove(session['user']['id'])
            online_connected_users -= 1
            socketio.emit('onlineUsers_server', {'number': online_connected_users}, broadcast=True)
    except:
        pass
    if 'user' in session:
        id = session['user']['id']
        session.pop('user', None)
        socketio.emit('del_user_server', {'id': id})
        DELETE('DELETE FROM online_tbl WHERE id={}'.format(id), 'online_tbl')
    resp = make_response(redirect('/'))
    resp.set_cookie('user', '', max_age=0)
    return resp


@app.route('/image/logo.jpg')
def load_logo():
    return send_file('image\\logo.jpg', mimetype='image/jpg')


@app.route('/image/css.png')
def load_image_css():
    return send_file('image\\css.png', mimetype='image/png')


def firstRun():
    global register_percentage_base, download_percentage_base
    DELETE('DELETE FROM online_tbl WHERE id>0', 'online_tbl')
    ds = SELECT("SELECT users, downloads FROM admin_tbl")
    for row in ds:
        register_percentage_base = row[0]
        download_percentage_base = row[1]
        break


def main():
    firstRun()
    socketio.run(app.run(ip, port=port, ssl_context=('server.crt', 'server.key'))) #debug=True))#, ssl_context=('server.crt', 'server.key')))


if __name__ == "__main__":
    main()


# DBCC CHECKIDENT (Users_Data_tbl, RESEED, 0)  <- this resets the id key index to 0
# 25/4/2020 1:18 PM
