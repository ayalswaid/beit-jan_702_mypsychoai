$(document).ready(function() {

    var socket = io.connect({{connectionStr}});

    //socket.on('{eventname}_{from}')

    //online dash page
    socket.on('newLogin_server', function(arg) {
        $("#connection_tbl").append('<tr id="' + arg['id'] + '"><td>' + arg['firstname'] + '</td></tr>');
    });

    socket.on('del_user_server', function(arg) {
        var row = document.getElementById(arg['id']);
        row.parentNode.removeChild(row);
    });

    //admin dash page
    socket.on('register_server', function(users) {
        var elem = document.getElementById('users_count');
        var percentage = document.getElementById('register_percentage');
        elem.innerText = users['number'];
        percentage.innerText = users['percentage'];
    });

    socket.on('download_server', function(users) {
        var elem = document.getElementById('downloads-count');
        var percentage = document.getElementById('download-percentage');
        elem.innerText = users['number'];
        percentage.innerText = users['percentage'];
    });

    socket.on('onlineUsers_server', function(number) {
        var elem = document.getElementById('online_users');
        elem.innerText = number['number'];
    });
});

