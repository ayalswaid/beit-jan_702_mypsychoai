import keras
from project.new.motivation_data import DataProcessor, data_generator

#input shape: (812042, 91)
# CTGenModel --> conditional text generation model
class CTGenModel:
    def __init__(self):
        """
        define our data specs and create our model.
        """
        self.input_shape = (812042, 91)
        self.total_words = 24001

        self.__create_model()

    def __create_model(self):
        """
        Build the RNN based model layers to build text generation model.
        layers: input -> GRU -> GRU -> Dense
        :return: none
        """
        embedding_dim = 100

        # building the model layers:
        self.model = keras.models.Sequential()

        # embedding layer
        self.model.add(keras.layers.Embedding(input_dim=self.total_words,
                                              output_dim=embedding_dim,
                                              input_length=self.input_shape[1]))

        # LSTM layer for RNN functionality
        self.model.add(keras.layers.Bidirectional(keras.layers.LSTM(256)))
        # self.model.add(keras.layers.LSTM(100, return_sequences=True))
        # self.model.add(keras.layers.LSTM(200))
        # GRU_1 = keras.layers.GRU(self.total_words, return_state=False, return_sequences=True)(embed_layer)
        # GRU_2 = keras.layers.GRU(self.total_words, return_state=True, return_sequences=True)(GRU_1)

        # dropout to prevent overfitting
        self.model.add(keras.layers.Dropout(0.6))

        # final Dense layer to make the output
        self.model.add(keras.layers.Dense(self.total_words, activation="softmax"))

        self.model.summary()
        self.model.compile(loss="categorical_crossentropy", metrics=["accuracy"], optimizer='adam')

    def train(self):
        """
        get the processed data from DataProcessor class and then fit the data by
        generator to prevent large memory usage. finally saves the model in h5 format
        :return: none
        """
        features, labels = DataProcessor().get_data()
        # print(features)
        self.model.fit_generator(data_generator(inputs=features, labels=labels, batch_size=154), steps_per_epoch=5273, nb_epoch=30)

        self.model.save("conditionalTxtGen_model+.h5")


if __name__ == "__main__":
    _m = CTGenModel()
    _m.train()
