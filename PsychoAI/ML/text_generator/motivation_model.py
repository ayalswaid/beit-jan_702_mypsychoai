import keras
import numpy as np
from SQL_Wrapper import sql
from numpy.random import seed
import random
import os

# tensorflow.compat.v1.set_random_seed(2)

seed(random.randint(0, 100000))

# TODO: load word index
class MotivationGen:
    __id2word = dict()

    def __init__(self):
        """
        load the trained model, and word2id dict from database.
        """

        file_name = os.path.dirname(__file__) + '\\conditionalTxtGen_model+.h5'

        self.__model = keras.models.load_model(file_name)

        self.__load_wordIndex()

    def __load_wordIndex(self):
        """
        load word_index from database.
        :return: None
        """
        cursor = sql.SELECT("SELECT * FROM textGen_word_index_tbl")

        for row in cursor:
            self.__id2word[row[0]] = row[1]
    def __encode(self, emotion):
        """
        encode text to sequence of integers
        :param emotion:
        :return: paded seq of numbers
        """
        # these values are from the conditionalTextGen+ wordIndex
        tag2id = {"happiness": 65, "fear": 146, "love": 14, "surprise": 2244, "anger": 825, "sadness": 553, "<PAD>": 0,
                  "<START>": 1, "<END>": 2, "<TAG>": 3}
        max_len = 91

        try:
            tag2id[emotion]
        except KeyError:
            raise Exception("error emotion not found: " + emotion)

        seq = [tag2id["<TAG>"], tag2id[emotion], tag2id["<TAG>"]]

        return keras.preprocessing.sequence.pad_sequences([seq], maxlen=max_len, padding="post")

    def __decode(self, seq):
        """
        seq to text .. this func is used after the model finish generating the motivation.
        :param seq: list of numbers
        :return: decoded: str
        """
        decoded = []
        for i in seq:
            decoded.append(self.__id2word[i])

        return " ".join(decoded)

    def __add_value(self, lst, value):
        """
        replace the value with the first padding element ( 0 ).
        :param lst: list to update
        :param value: to replace with the padding element ( 0 ).
        :return: updated list
        """
        for i in range(len(lst[0])):
            if lst[0][i] == 0:
                lst[0][i] = value
                break
        return lst

    def __distribute_probabilites(self, probabilities, highest_n):
        """
        this function get a probabilities list and distribute the probabilities so the highest
        highest_n(int) probabilities still in the the list and every probability else is zero.
        this task is done by summing unused probabilities and zero it. then add this sum to all
        used probabilities(not zero). and then we will get another list that contain new
        probabilities and their sum is 1.
        :param probabilities: probabilities list
        :param highest_n: the number of largest probabilities.
        :return: new probabilities list at the same size as the input list.
        """
        # get the highest probabilities
        highest_probs = (-probabilities).argsort()[:highest_n]
        probabilities_to_distribute = 0

        for i in range(len(probabilities)):
            if i in highest_probs:
                highest_probs = highest_probs[highest_probs != i]

            else:
                probabilities_to_distribute += probabilities[i] / highest_n
                probabilities[i] = 0


        for i in range(len(probabilities)):
            if probabilities[i] != 0:
                probabilities[i] += probabilities_to_distribute
        return probabilities

    def __trim(self, str):
        """
        to clean the final generated text from tokens
        :param str: string you want to trim
        :return: only the motivation without the tokkens
        """
        try:
            start = str.find('<START>') + 7
            end = str.find('<END>')
            return str[start:end]
        except:
            return str

    def generate(self, emotion: str):
        """
        generate motivation by self.__model depending on the emotion parameter
        :param emotion:str --> the generator will build a motivation depending on this emotion.
        there are 6 emotions and it is the same as the emotion recognition output(to give better results)
        emotions:  happiness, fear, love, surprise, anger, sadness.
        the generator generates every word from the highest 5 probabilities of model output.
        :return: str --> motivation to the emotion parameter
        """
        encoded = self.__encode(emotion)

        output = 0
        while output != 2:
            output = self.__model.predict(encoded)[0] # list of probabilities

            # get the highest 10 elements(probilities) of the output.
            output = self.__distribute_probabilites(output, highest_n=2)

            output = np.random.choice(a=len(output), size=1, p=output)[0]

            encoded = self.__add_value(encoded, output)

        return self.__trim(self.__decode(encoded[0]))


def generate(emotion):
    _generator = MotivationGen()
    return _generator.generate(emotion)

def main():
    _m = MotivationGen()
    print(_m.generate("anger"))


if __name__ == "__main__":
    main()
