import json
from keras.preprocessing.text import Tokenizer
import keras
import string
# from project.new.emotion_recognition.model_v16 import Sentemint_Mia
import random
from keras.preprocessing.sequence import pad_sequences
# import numpy as np
from SQL_Wrapper.sql import INSERT
from SQL_Wrapper import sql
from ML.Version16.model_v16 import Sentemint_Mia

# this class is to read and process the dataset data.
# *Note: this class will not return the final version of the
# data(issue is explained in DataProcessor.get_data). the final version will be converted by
# the data generator
class DataProcessor:
    __data = {"happiness": [],
              "fear": [],
              "love": [],
              "surprise": [],
              "anger": [],
              "sadness": []}

    def __init__(self):
        """
        first read the data from the json dataset and then send it function that
        process the data and tokenize it.
        """
        self.__load_data()

        self.__tokenize()

    def __load_data(self):
        """
        read the json file and convery it to dict and then call function to sum the
        tags of each quote
        :return: none
        """
        with open('motivation_dataset.json', "r", encoding='utf8') as file:
            data = json.loads(file.read())
            # total quotes: 48391
            self.__distribute_data(data)

            # self.__emotion_relation_creator(data)

    def __emotion_relation_creator(self, data):
        """
        reduce the number of the tags using the emotion recognition NN.
        first: it will give us larger number of motivations in the final dataset.
        secondly: building a relation with the emotion rec' may help our system to get
        more accurate
        :return: None
        """
        rec_model = Sentemint_Mia()
        tag2emotion = dict()  # cache dict contain each tag and its emotion.

        for dct in data:
            current_emotions = []  # the emotions for current motivation
            for tag in dct["Tags"]:
                tag = self.__clean_text(tag)
                print("tag: ", tag)
                print("current emotions: ", current_emotions)
                if tag[-1] == " ":  # issue with our dataset
                    tag = tag[:-1]

                # get the emotion in the tag
                if tag not in tag2emotion.keys():
                    tag_emotion = rec_model.predict(tag)
                    tag2emotion[tag] = tag_emotion
                else:
                    tag_emotion = tag2emotion[tag]

                if tag_emotion not in current_emotions:
                    current_emotions.append(tag_emotion)
                    self.__data[tag_emotion].append(self.__clean_text(dct["Quote"]))

            print(tag2emotion)

        print("happiness:", len(self.__data["happiness"]), "\nfear:", len(self.__data["fear"]), "\nlove:",
              len(self.__data["love"]),
              "\nsurprise:", len(self.__data["surprise"]), "\nsadness:", len(self.__data["sadness"]), "\nanger:",
              len(self.__data["anger"]))

    def __distribute_data(self, data):
        """
        distributing motivations on emotions by checking some keywords in the tag. finally store
        them in self.__data: dict()
        results: total quotes: 48391
                happiness: 8831 quotes
                fear: 574 quotes
                love: 10851 quotes
                surprise: 6485 quotes
                sadness: 609 quotes
                anger: 5358quotes
        :parameter: data: json file converted to dict()
        :return: none
        """
        # total quotes: 48391
        last = ""
        for dct in data:
            if dct["Quote"] == last:  # to prevent duplications
                continue
            last = dct["Quote"]

            dct["Tags"] = [x.lower() for x in dct["Tags"]]
            if "humor" in dct['Tags'] or "humor " in dct['Tags'] or "Funny" in dct['Tags'] or "Funny " in dct['Tags']:
                self.__data["happiness"].append(self.__clean_text(dct["Quote"]))

            if "be-yourself" in dct['Tags'] or "be-yourself " in dct['Tags'] or "confidence" in dct[
                'Tags'] or "confidence " in dct['Tags'] or "fear" in dct['Tags'] or "fear " in dct['Tags']:
                self.__data["fear"].append(self.__clean_text(dct["Quote"]))

            if "love" in dct['Tags'] or "love " in dct['Tags']:
                self.__data["love"].append(self.__clean_text(dct["Quote"]))

            if "inspirational" in dct['Tags'] or "inspirational " in dct['Tags']:
                self.__data["surprise"].append(self.__clean_text(dct["Quote"]))

            if "smile" in dct['Tags'] or "smile " in dct['Tags'] or "sadness" in dct['Tags'] or "sadness " in dct[
                'Tags'] or "optimism" in dct['Tags'] or "optimism " in dct['Tags'] or "cry" in dct['Tags'] or "cry " in \
                    dct['Tags'] or "sad" in dct['Tags'] or "sad " in dct['Tags']:
                self.__data["sadness"].append(self.__clean_text(dct["Quote"]))

            if "happiness" in dct['Tags'] or "happiness " in dct['Tags'] or "Creativity" in dct[
                'Tags'] or "Creativity " in dct['Tags']:
                self.__data["anger"].append(self.__clean_text(dct["Quote"]))

        print("happiness:", len(self.__data["happiness"]), "\nfear:", len(self.__data["fear"]), "\nlove:",
              len(self.__data["love"]),
              "\nsurprise:", len(self.__data["surprise"]), "\nsadness:", len(self.__data["sadness"]), "\nanger:",
              len(self.__data["anger"]))

    def __tokenize(self):
        """
        initiate the word index and set a token for each word .. plus adding special tokens:
        <PAD> --> padding token
        <START> --> indicate to starting a sequence
        <END> --> to end the sequence
        <TAG> --> this token appears tiwce in each sequence with the template [<TAG> tag <TAG>] while tag is the condition of the sequence

        last results:
        input shape: (812042, 91)
        labels shape: (812042, 1)
        :return:
        """
        texts = self.__data["happiness"]
        texts.extend(self.__data["fear"])
        texts.extend(self.__data["love"])
        texts.extend(self.__data["surprise"])
        texts.extend(self.__data["anger"])
        texts.extend(self.__data["sadness"])
        tokenizer = Tokenizer(num_words=10000)
        tokenizer.fit_on_texts(texts)

        del texts

        for k, v in tokenizer.word_index.items():
            tokenizer.word_index[k] = v + 3

        tokenizer.word_index['<PAD>'] = 0
        tokenizer.word_index['<START>'] = 1
        tokenizer.word_index['<END>'] = 2
        tokenizer.word_index['<TAG>'] = 3

        self.__word2id = tokenizer.word_index
        self.total_words = len(self.__word2id) + 1  # 18073

        # insert word2id to the database
        for k,v in self.__word2id.items():
            qry = "INSERT INTO textGen_word_index_tbl (id, word) VALUES ({}, '{}')".format(v, k)
            INSERT(qry, "textGen_word_index_tbl")
        print("total words: ", self.total_words)
        # tag2id = {"happiness": 65, "fear":146, "love": 14, "surprise":2244,"anger":825, "sadness":553}

        # add tokens to the data
        # max_len = 0  # for padding
        # data_list = []  # template: [(label,sentence), (label,sentence), ...]. this var is for shuffling the data later
        #
        # for tag, motivations in self.__data.items():
        #     for line in motivations:
        #         #  encode the seq and add special tokens to it
        #         x_label = self.__encode(str('<TAG> ' + tag + ' <TAG> ' + ' <START> ' + line.lower() + ' <END>').split())
        #
        #         # find max length for padding
        #         if len(x_label) > max_len:
        #             max_len = len(x_label)
        #
        #         #  split the line to n-gram template
        #         for i in range(2, len(x_label) - 1):
        #             data_list.append(([x_label[i + 1]], x_label[:i + 1]))  # [(label,seq),...]
        #
        # # shuffle the data
        # random.shuffle(data_list)
        #
        # # unzip the data
        # self.input_sequences = [x[1] for x in data_list]
        # self.labels = [x[0] for x in data_list]
        # del data_list
        #
        # #  pad the input data
        # self.input_sequences = pad_sequences(self.input_sequences, maxlen=max_len, padding="post", dtype="float32")
        # self.max_len = max_len  # 91
        #
        #
        # print("input shape:", self.input_sequences.shape)
        # print("max len shape:", self.max_len)

    def __encode(self, vec):
        """
        convert vec of strings to vector of integers(fucking encodeing)
        :param vec: list of strings
        :return: list of integers
        """
        try:
            return [self.__word2id[x] for x in vec]
        except:
            print(vec)
        return [self.__word2id[x] for x in vec]

    def __clean_text(self, txt: str):
        """
            clean text (str) from !@#$%^& etc. to yield better results
            :param: txt: string to clean
            :return: string
        """

        txt = txt.replace(":", " ")
        txt = txt.replace(".", " ")
        txt = txt.replace("-", " ")

        return "".join([c for c in txt if c not in string.punctuation])

    def get_data(self):
        """
        return the input seq and its labels arrays.
        *Note: this function return the labels with shape (total_inputs, 1) and does not
        convert it to one-hot vector because of its large memory size, since its shape would be
        (total_inputs, total_words=24001, nb_classes=6). so later we use generator to train the NN.
        :return: self.input_sequences--> shape(812042, 91),
                 self.labels --> shape(812042,1)
        """
        return self.input_sequences, self.labels

def data_generator(inputs, labels, batch_size):
    """
    this function is used to keras.fit_generator .. to fix the large sized data issue we send each batch
    to train(to the memory) and then remove it and adding the next batch ...
    :param inputs: input_sequences: tensor --> from the DataProcessor class.
    :param labels: input_Sequences labels --> also from DataProcessor class.
    :param batch_size: training batch size .
    :return: yield each batch of data.
    """
    # input shape (812042, 91)
    # max_len 91
    total_words = 24001

    current_batch_num = 0  # current batch in training epoch
    while True:
        batch_inputs = inputs[current_batch_num:current_batch_num+batch_size]
        batch_labels = labels[current_batch_num:current_batch_num+batch_size]

        #  convert labels to one-hot vector before yielding
        yield batch_inputs, keras.utils.to_categorical(batch_labels, num_classes=total_words)

        current_batch_num += batch_size

        #  check if the epoch is finished
        if current_batch_num == len(inputs):
            current_batch_num = 0


def main():
    d = DataProcessor()


if __name__ == '__main__':
    main()
