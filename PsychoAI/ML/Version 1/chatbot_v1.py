import numpy as np
import tensorflow as tf
import pandas as pd
from tensorflow.python.keras.layers import Input, LSTM, Dense
from tensorflow.keras.layers import Embedding, TimeDistributed

MAX_LEN = 300
VOCAB_SIZE = None

def seq2seq_model_builder(HIDDEN_DIM=300):

    encoder_inputs = Input(shape=(MAX_LEN, ), dtype='int32',)
    encoder_embedding = Embedding(encoder_inputs)
    encoder_LSTM = LSTM(HIDDEN_DIM, return_state=True)
    encoder_outputs, state_h, state_c = encoder_LSTM(encoder_embedding)

    decoder_inputs = Input(shape=(MAX_LEN,), dtype='int32', )
    decoder_embedding = Embedding(decoder_inputs)
    decoder_LSTM = LSTM(HIDDEN_DIM, return_state=True, return_sequences=True)
    decoder_outputs, _, _ = decoder_LSTM(decoder_embedding, initial_state=[state_h, state_c])

    # dense_layer = Dense(VOCAB_SIZE, activation='softmax')
    outputs = TimeDistributed(Dense(VOCAB_SIZE, activation='softmax'))(decoder_outputs)
    model = Model([encoder_inputs, decoder_inputs], outputs)

    return model


































# import numpy as np
# import tensorflow as tf
# # tf.enable_eager_execution()
# import helpers
#
# tf.reset_default_graph()
# sess = tf.InteractiveSession()
# # tf.map_fn()
#
# print(tf.__version__)
#
# PAD = 0
# EOS = 1
#
# vocab_size = 10
# input_embedding_size = 20
#
# encoder_hidden_units = 20
# decoder_hidden_units = encoder_hidden_units * 2
#
# encoder_inputs = tf.placeholder(shape=(None, None), dtype=tf.int32, name='encoder_inputs')
# encoder_inputs_length = tf.placeholder(shape=(None,), dtype=tf.int32, name='encoder_inputs_length')
# decoder_targets = tf.placeholder(shape=(None, None), dtype=tf.int32, name='decoder_targets')
#
# embeddings = tf.Variable(tf.random_uniform([vocab_size, input_embedding_size], -1.0, 1), dtype=tf.float32)
#
# encoder_input_embedded = tf.nn.embedding_lookup(embeddings, encoder_inputs)
#
# from tensorflow.python.ops.rnn_cell import LSTMCell, LSTMStateTuple
#
# encoder_cell = LSTMCell(encoder_hidden_units)
#
# ((encoder_fw_outputs,
#   encoder_bw_outputs),
#  (encoder_fw_final_state,
#   encoder_bw_final_state)) = (
#     tf.nn.bidirectional_dynamic_rnn(cell_fw=encoder_cell,
#                                     cell_bw=encoder_cell,
#                                     inputs=encoder_input_embedded,
#                                     sequence_length=encoder_inputs_length,
#                                     dtype=tf.float32, time_major=True)
# )
#
#
# encoder_final_state_c = tf.concat((encoder_fw_final_state.c, encoder_bw_final_state.c), 1)
# encoder_final_state_h = tf.concat((encoder_fw_final_state.h, encoder_bw_final_state.h), 1)
# encoder_final_state = LSTMStateTuple(
#     c=encoder_final_state_c,
#     h=encoder_final_state_h
# )
#
# decoder_cell = LSTMCell(decoder_hidden_units)
# encoder_max_time, batch_size = tf.unstack(tf.shape(encoder_inputs))
# decoder_lengths = encoder_inputs_length + 3
#
# w = tf.Variable(tf.random_uniform([decoder_hidden_units], vocab_size, -1, 1), dtype=tf.float32)
# b = tf.Variable(tf.zeros([vocab_size]), dtype=tf.float32)
#
# assert EOS == 1 and PAD == 0
#
# eos_time_slice = tf.ones([batch_size], dtype=tf.int32, name='EOS')
# pad_time_slice = tf.zeros([batch_size], dtype=tf.int32, name='PAD')
#
#
# eos_step_embedded = tf.nn.embedding_lookup(embeddings, eos_time_slice)
# pad_step_embedded = tf.nn.embedding_lookup(embeddings, pad_time_slice)
#
# def loop_fn_initial():
#     initial_elements_finished = (0 >= decoder_lengths)
#
#     initial_input = eos_step_embedded
#
#     initial_cell_state = encoder_final_state
#
#     initial_cell_output = None
#
#     initial_loop_state = None
#
#     return (
#         initial_elements_finished,
#         initial_input,
#         initial_cell_state,
#         initial_cell_output,
#         initial_loop_state
#     )
#
#
# def loop_fn_transition(time, previous_output, previous_state, previous_loop_state):
#
#     def get_next_input():
#         output_logits = tf.add(tf.matmul(list(previous_output), w), b)
#
#         prediction = tf.argmax(output_logits, axis=1)
#
#         next_input = tf.nn.embedding_lookup(embeddings, prediction)
#
#         return next_input
#
#     elements_finished = (time >= decoder_lengths)
#
#     finished = tf.reduce_all(elements_finished)
#
#     input = tf.cond(finished, lambda: pad_step_embedded, get_next_input)
#
#     state = previous_state
#     output = previous_output
#     loop_state = None
#
#     return (
#         elements_finished,
#         input,
#         state,
#         output,
#         loop_state
#     )
#
#
# def loop_fn(time, previous_output, previous_state, previous_loop_state):
#     if previous_state is None:
#         assert previous_output is None and previous_state is None
#         return loop_fn_initial()
#     else:
#         return loop_fn_transition(time, previous_output, previous_state, previous_loop_state)
#
#
# decoder_outputs_ta, decoder_final_state, _ = tf.nn.raw_rnn(decoder_cell, loop_fn)
# decoder_outputs = decoder_outputs_ta.stack()
#
# print(decoder_outputs)
#
# decoder_max_steps, decoder_batch_size, decoder_dim = tf.unstack(tf.shape(decoder_outputs))
#
# decoder_outputs_flat = tf.reshape(decoder_outputs, (-1, decoder_dim))
#
# decoder_logits_flat = tf.add(tf.matmul(decoder_outputs_flat, w), b)
#
# decoder_logits = tf.reshape(decoder_logits_flat, (decoder_max_steps, decoder_batch_size, vocab_size))
#
# decoder_prediction = tf.argmax(decoder_logits, 2)
#
# print(decoder_prediction)
#
# stepwise_cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
#     labels=tf.one_hot(decoder_targets, depth=vocab_size, dtype=tf.float32),
#     logits=decoder_logits
# )
#
# loss = tf.reduce_mean(stepwise_cross_entropy)
# train_op = tf.train.AdamOptimizer().minimize(loss)
#
# sess.run(tf.global_variables_initializer())
#
#
#
# batch_size = 100
#
# batches = helpers.random_sequences(length_from=3, length_to=8,
#                                    vocab_lower=2, vocab_upper=10,
#                                    batch_size=batch_size)
#
# print('head of batch')
# for seq in next(batches)[:10]:
#     print(seq)
#
#
#
#
#
#
#
# def next_feed():
#     batch = next(batches)
#     encoder_inputs_, encoder_inputs_length_ = helpers.batch(batch)
#     decoder_targets_, _ = helpers.batch([(sequence) + (EOS) + (PAD) for sequence in batch])
#     return {
#         encoder_inputs: encoder_inputs_,
#         encoder_inputs_length: encoder_inputs_length_,
#         decoder_targets: decoder_targets_,
#     }
#
# loss_track = []
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
