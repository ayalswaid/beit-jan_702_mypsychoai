import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import keras
from SQL_Wrapper.sql import SELECT, INSERT
from keras.preprocessing.sequence import pad_sequences


# max_words = 178
class Model:
    # Initialize word2id and label2id dictionaries that will be used to encode words and labels

    # encode words into id's {word: id}
    word2id = dict()

    # encode labels into id's {label: id}
    label2id = {"anger": 0,
                "surprise": 1,
                "sadness": 2,
                "happiness": 3,
                "fear": 4,
                "love": 5}

    X = list()
    Y = list()

    def __init__(self):
        """
            initiate our model and import the data and convert it to the workable format.
        """

        self.__load_index_data()

        # loading the data from the dataset and prepairing the wordIndex
        self.__load_train_data()

        # encode our text data to vectors of numbers to send it to the neural network
        self.__encode_data()

        # create the model and compile it
        self.__create_model()

    def __load_index_data(self):
        """
            load the word dict, with its id {word, id}
        :return: None
        """
        cursor = SELECT("SELECT * FROM word_index_tbl")

        for row in cursor:
            self.word2id[row[1]] = row[0]

    def __load_train_data(self):
        """
            load the dataset, the sentences to train the model and the labels to each sentence
            our data is saved to self.X and self.Y
            :return: None
        """
        # Loading the dataset
        dataset = pd.read_csv("emotion.data")

        # input sentences is the X to train the network
        self.input_sentences = [text.split(" ") for text in dataset["text"].values.tolist()]


        # labels is the Y to the X
        self.labels = dataset["emotions"].values.tolist()

        max_words = 0  # maximum number of words in a sentence

        # Construction of word2id dict
        for sentence in self.input_sentences:
            for word in sentence:
                # If length of the sentence is greater than max_words, update max_words
                if len(sentence) > max_words:
                    max_words = len(sentence)

        self.max_words = max_words

    def get_key(self, val, dict):
        for key, value in dict.items():
            if val == value:
                return key
        return 0

    def __encode_data(self):

        self.X = [[self.word2id[word.replace("'", "")] for word in sentence] for sentence in self.input_sentences]
        self.Y = [self.label2id[label] for label in self.labels]

        # Apply Padding to X
        print("max words", self.max_words)
        self.X = pad_sequences(self.X, self.max_words)

        # Convert Y to numpy array
        self.Y = keras.utils.to_categorical(self.Y, num_classes=len(self.label2id), dtype='float32')

        # Print shapes
        print("Shape of X: {}".format(self.X.shape))
        print("Shape of Y: {}".format(self.Y.shape))
        print("length of word2id : {}".format(len(self.word2id)))


    def __create_model(self):
        """
            build our model and set each layer that the model will use. our layers is imported from Keras package.
            the model is compiled and then saved to self.model
            :return: None
        """
        embedding_dim = 100 # The dimension of word embeddings

        # Define input tensor
        sequence_input = keras.Input(shape=(self.max_words,), dtype='int32')

        # Word embedding layer
        # len(self.word2id) + 1 -> there's few missing word
        embedded_inputs = keras.layers.Embedding(351602, embedding_dim, input_length=self.max_words)(sequence_input)

        # Apply dropout to prevent overfitting
        embedded_inputs = keras.layers.Dropout(0.2)(embedded_inputs)

        # Apply Bidirectional LSTM over embedded inputs
        lstm_outs = keras.layers.wrappers.Bidirectional(keras.layers.LSTM(embedding_dim, return_sequences=True))(embedded_inputs)

        # Apply dropout to LSTM outputs to prevent overfitting
        lstm_outs = keras.layers.Dropout(0.2)(lstm_outs)

        # Attention Mechanism - Generate attention vectors
        input_dim = int(lstm_outs.shape[2])
        permuted_inputs = keras.layers.Permute((2, 1))(lstm_outs)
        attention_vector = keras.layers.TimeDistributed(keras.layers.Dense(1))(lstm_outs)
        attention_vector = keras.layers.Reshape((self.max_words,))(attention_vector)
        attention_vector = keras.layers.Activation('softmax', name='attention_vec')(attention_vector)
        attention_output = keras.layers.Dot(axes=1)([lstm_outs, attention_vector])

        # Last layer: fully connected with softmax activation
        fc = keras.layers.Dense(embedding_dim, activation='relu')(attention_output)
        output = keras.layers.Dense(len(self.label2id), activation='softmax')(fc)

        # Finally building model
        self.model = keras.Model(inputs=[sequence_input], outputs=output)
        self.model.compile(loss="categorical_crossentropy", metrics=["accuracy"], optimizer='adam')

        # Print model summary
        print(self.model.summary())

    def fit(self):
        """
            train our model(self.model) on our data(self.X, self.Y), and then save the trained model in "mia.model"
            file for later usage
            :return: None
        """
        self.model.fit(self.X, self.Y, epochs=3, batch_size=64, validation_split=0.1, shuffle=True, verbose=1)

        self.model.save('mia_pos.model')


if __name__ == '__main__':
    model = Model()
    model.fit()



#     def __load_data(self):
#         """
#             load the dataset, generate wordIndex and label2id and get the max number of words in a sentence.
#             our data is saved to self.X and self.Y
#             :return: None
#         """
#         # Loading the dataset
#         dataset = pd.read_csv("emotion.data")
#
#
#         input_sentences = [text.split(" ") for text in dataset["text"].values.tolist()]
#         labels = dataset["emotions"].values.tolist()
#
#         # Initialize word2id and label2id dictionaries that will be used to encode words and labels
#         word2id = dict()
#         label2id = dict()
#
#         max_words = 0  # maximum number of words in a sentence
#
#         # Construction of word2id dict
#         for sentence in input_sentences:
#             for word in sentence:
#                 # Add words to word2id dict if not exist
#                 if word not in word2id:
#                     word2id[word] = len(word2id)
#             # If length of the sentence is greater than max_words, update max_words
#             if len(sentence) > max_words:
#                 max_words = len(sentence)
#
#         self.word2id = word2id
#         self.max_words = max_words
#
#         # insert word2id dict into database for later usage
#         # for k, v in self.word2id.items():
#             # sql.INSERT("insert into word_index_tbl(id, word) VALUES({}, {})".format(v, k),
#                         # "word_index_tbl")
#
#         self.label2id = {l: i for i, l in enumerate(set(labels))}
#
#         # insert label2id dict into database for later usage
#         # for k, v in self.label2id.items():
#             # sql.INSERT("INSERT INTO label_index_tbl (id, label) VALUES({},'{}')".format(v, k), "label_index_tbl")
#
#         # Construction of label2id and id2label dicts
#         # self.label2id = {l: i for i, l in enumerate(set(labels))}
#         self.id2label = {v: k for k, v in label2id.items()}
#
#         self.input_sentences = input_sentences
#         self.labels = labels
#
#     def __encode_data(self):
#         """
#             encode the data, change the sequences to vectors of numbers dependeing on the wordIndex made
#              in the __load_data func. this function update the encoded data to self.X and self.Y
#              *Note: each word has a single id and this id is only for this word.
#             :return: None
#         """
#         # Encode input words and labels
#         self.X = [[self.word2id[word] for word in sentence] for sentence in self.input_sentences]
#         self.Y = [self.label2id[label] for label in self.labels]
#
#         # Apply Padding to X
#         print("max words", self.max_words)
#         self.X = pad_sequences(self.X, self.max_words)
#
#         # Convert Y to numpy array
#         self.Y = keras.utils.to_categorical(self.Y, num_classes=len(self.label2id), dtype='float32')
#
#         # Print shapes
#         print("Shape of X: {}".format(self.X.shape))
#         print("Shape of Y: {}".format(self.Y.shape))
#
#
#
#     def __load_wordIndex(self):
#         """
#             load the wordIndex from the database and save it to self.wordIndex: dict
#             :param: None
#             :return: None
#         """
#         data = sql.SELECT("SELECT * FROM word_index_tbl")
#
#         self.w2id = dict()
#         for e in data:
#             self.w2id[e[0]] = e[1]
#
#     def __load_labelIndex(self):
#         """
#             load the labelIndex from the database and save it to self.wordIndex: dict
#             {int id: str word}
#             :return: None
#         """
#
#         data = sql.SELECT("SELECT * FROM label_index_tbl")
#
#         self.id2label = dict()
#         for e in data:
#             self.id2label[e[0]] = e[1]
#
#     def encode(self, sequence):
#         """
#             convert a sequence of words to numbers depending on the wordIndex
#             :param sequence: vector of words/str
#             :return: vector of numbers/int
#         """
#         arr = []
#         for word in sequence:
#             arr.append(self.get_key(word, self.w2id))
#         return arr
#         # return [self.w2id.get(word, -1) for word in sequence]
#
#     def get_key(self, val, dict):
#         for key, value in dict.items():
#             if val == value:
#                 return key
#         return 0
#
#
#
#     def __create_model(self):
#         """
#             build our model and set each layer that the model will use. our layers is imported from Keras package.
#             the model is compiled and then saved to self.model
#             :return: None
#         """
#         embedding_dim = 100 # The dimension of word embeddings
#
#         # Define input tensor
#         sequence_input = keras.Input(shape=(self.max_words,), dtype='int32')
#
#         # Word embedding layer
#         embedded_inputs =keras.layers.Embedding(len(self.word2id) + 1,
#                                                 embedding_dim,
#                                                 input_length=self.max_words)(sequence_input)
#
#         # Apply dropout to prevent overfitting
#         embedded_inputs = keras.layers.Dropout(0.2)(embedded_inputs)
#
#         # Apply Bidirectional LSTM over embedded inputs
#         lstm_outs = keras.layers.wrappers.Bidirectional(
#             keras.layers.LSTM(embedding_dim, return_sequences=True)
#         )(embedded_inputs)
#
#         # Apply dropout to LSTM outputs to prevent overfitting
#         lstm_outs = keras.layers.Dropout(0.2)(lstm_outs)
#
#         # Attention Mechanism - Generate attention vectors
#         input_dim = int(lstm_outs.shape[2])
#         permuted_inputs = keras.layers.Permute((2, 1))(lstm_outs)
#         attention_vector = keras.layers.TimeDistributed(keras.layers.Dense(1))(lstm_outs)
#         attention_vector = keras.layers.Reshape((self.max_words,))(attention_vector)
#         attention_vector = keras.layers.Activation('softmax', name='attention_vec')(attention_vector)
#         attention_output = keras.layers.Dot(axes=1)([lstm_outs, attention_vector])
#
#         # Last layer: fully connected with softmax activation
#         fc = keras.layers.Dense(embedding_dim, activation='relu')(attention_output)
#         output = keras.layers.Dense(len(self.label2id), activation='softmax')(fc)
#
#         # Finally building model
#         self.model = keras.Model(inputs=[sequence_input], outputs=output)
#         self.model.compile(loss="categorical_crossentropy", metrics=["accuracy"], optimizer='adam')
#
#         # Print model summary
#         print(self.model.summary())
#
#     def fit(self):
#         """
#             train our model(self.model) on our data(self.X, self.Y), and then save the trained model in "mia.model"
#              file for later usage
#             :return: None
#         """
#         self.model.fit(self.X, self.Y, epochs=2, batch_size=64, validation_split=0.1, shuffle=True, verbose=1)
#
#         self.model.save('mia.model')
#
#
# if __name__ == '__main__':
#     model = Model()
#     model.fit()
