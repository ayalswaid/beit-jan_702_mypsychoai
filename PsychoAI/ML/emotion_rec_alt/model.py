from __future__ import absolute_import, division, print_function, unicode_literals
import os

import keras
from keras.preprocessing.sequence import pad_sequences
from SQL_Wrapper import sql
import numpy as np


# TODO: set path for model.load ... train and checkpoint
# Note: our Ai is named Mia
class Mia:
    __model = None
    __max_words = 830
    word2id = None
    id2label = None

    label2id = {'sadness': 0,
                'love': 1,
                'anger': 2,
                'surprise': 3,
                'fear': 4,
                'happiness': 5}# 6 labels

    def __init__(self):
        """
            load our saved model and save it to model var.
            :param: None.
            :return: None
        """

        # load the wordIndex dict from the database
        self.__load_wordIndex()



        # load the model
        file_name = os.path.dirname(__file__) + '\\miaPlus.model'
        self.__model = keras.models.load_model(file_name)


    def __load_wordIndex(self):
        """
            load the wordIndex from the database and save it to self.wordIndex: dict
            {int id: str word}
            :param: None
            :return: None
        """
        data = sql.SELECT("SELECT * FROM word_index_tbl")

        self.word2id = dict()
        for e in data:
            self.word2id[e[1]] = e[0]

    def __load_model(self):
        """
        load the model from the checkpoints
        :return: None
        """

        # model = self.__create_model()


    def create_model(self):
        """
        create the lstm model, compile and save it to self.model
        :return:
        """

        embedding_dim = 100
        # Define input tensor
        sequence_input = keras.Input(shape=(self.__max_words,), dtype='int32')

        # Word embedding layer
        embedded_inputs = keras.layers.Embedding(len(self.word2id) + 1,
                                                 embedding_dim,
                                                 input_length=self.__max_words)(sequence_input)

        # Apply dropout to prevent overfitting
        embedded_inputs = keras.layers.Dropout(0.2)(embedded_inputs)

        # Apply Bidirectional LSTM over embedded inputs
        lstm_outs = keras.layers.wrappers.Bidirectional(
            keras.layers.LSTM(embedding_dim, return_sequences=True)
        )(embedded_inputs)

        # Apply dropout to LSTM outputs to prevent overfitting
        lstm_outs = keras.layers.Dropout(0.2)(lstm_outs)

        # Attention Mechanism - Generate attention vectors
        attention_vector = keras.layers.TimeDistributed(keras.layers.Dense(1))(lstm_outs)
        attention_vector = keras.layers.Reshape((self.__max_words,))(attention_vector)
        attention_vector = keras.layers.Activation('softmax', name='attention_vec')(attention_vector)
        attention_output = keras.layers.Dot(axes=1)([lstm_outs, attention_vector])

        # Last layer: fully connected with softmax activation
        fc = keras.layers.Dense(embedding_dim, activation='relu')(attention_output)
        output = keras.layers.Dense(len(self.label2id), activation='softmax')(fc)

        # Finally building model
        model = keras.Model(inputs=[sequence_input], outputs=output)
        model.compile(loss="categorical_crossentropy", metrics=["accuracy"], optimizer='adam')

        return model

    def __load_labelIndex(self):
        """
            load the labelIndex from the database and save it to self.wordIndex: dict
            :return: None
        """

        data = sql.SELECT("SELECT * FROM label_index_tbl")

        self.id2label = dict()
        for e in data:
            self.id2label[e[0]] = e[1]

    def encode(self, sequence):
        """
            convert a sequence of words to numbers depending on the wordIndex
            :param sequence: vector of words/str
            :return: vector of numbers/int
        """
        new_seq = [self.word2id.get(word+"\n", self.word2id['<UNK>\n']) for word in sequence]
        # return arr
        return new_seq

    def __get_key(self, id, dict):
        """

        :param val:
        :param dict:
        :return:
        """
        for key, value in dict.items():
            if id == value:
                return key
        return -1
        # raise Exception("IdNotFound Error: id "+ str(id) +" is not found in label2id dict")

    def predict(self, text):
        """
            take the user's problem as an input, convert it to relevant vector,
            return the predicted sentiment.
            :param text: string
            :return: sentiment: text
        """

        # string to vector of numbers by wordIndex
        vec = [self.encode(text.split())]


        # pad to the expected input length
        vec = pad_sequences(vec, self.__max_words, padding='post')

        # get the sentiment id from the model
        label = self.__model.predict(vec)

        # create a one-hot vector while the max element is 1.
        decoded = [max(label[0]) == i for i in label]

        # find the index where the 1 element
        index = [np.where(r == 1)[0][0] for r in decoded][0]

        # turn the sentiment id into text
        emotion = self.__get_key(index, self.label2id)
        return emotion

    def train(self, x_labels, y_labels):
        """
            in case that our predicted sentiment is wrong... this func' gives Mia the x_labels and y_labels
            to train to get more accurate.
            :param x_labels: input for neural network: list of strings.
            :param y_labels: the output of the neural network: list of the classes
            :return: None
        """

        # string to vector of numbers by wordIndex
        x_labels = [self.encode(seq.split()) for seq in x_labels]

        # pad to the expected input length
        x_labels = pad_sequences(x_labels, self.__max_words, padding='post')
        new_y_labels = []
        for i in y_labels:
            new_y_labels.append(self.label2id[i])

        y_labels = keras.utils.to_categorical(new_y_labels, len(self.label2id), dtype='float32')

        return x_labels, y_labels
        # load the checkpoint model

        checkpoint_path = "cp-{epoch:04d}.ckpt"
        checkpoint_dir = os.path.dirname(checkpoint_path)

        cp_callback = keras.callbacks.ModelCheckpoint(checkpoint_path,
                                                      verbose=1,
                                                      save_weights_only=True)
        self.__model.fit(x_labels, y_labels, epochs=2, callbacks=[cp_callback], batch_size=2, validation_split=0.1, shuffle=True, verbose=1)


def guess(text):
    _mia = Mia()
    return _mia.predict(text)

def main():
    _mia = Mia()
    # v = _mia.train(["today is my birthday", "i find out that she was my mom", "i really respect him", "they are my second family", "they are always insulting me", "i hate them", "i hate him", "they hate me", "everyone hates me", "today is my day", "she will not let me go", "i think i am losing my mind", "they did nothing","i am the king of fucking everything", "i am lost", "i am the king of everything", "at the end i found the money in my pocket", "we are like two birds together", "it is nice but it is not clear", "i want to smoke on my way", "nobody can beat me", "they put the blame on me", "i think i was thinking that i am stupid but they said i am not", "he did not take much time to find it", "i think it will be hard", "she lied on me", "she was lying all the time and i did not notice", "i found that my boyfriend was always cheating on me", "it is the first time that i hear that", "i do not want to go out because of the rain"], ['joy', 'surprise', 'love', 'love', 'anger', 'anger', 'anger', 'sadness', 'sadness', 'joy', 'joy', "anger", "anger", "joy", "sadness", "joy", 'surprise', 'love', 'joy', 'joy', "joy", "anger", "surprise", "joy", "fear", "sadness", "anger", "surprise", "surprise", "fear"])
    while True:
        print(_mia.predict(input()))


# def start():
#     os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
#     _mia = Mia()
#     return _mia

if __name__ == "__main__":
    main()
