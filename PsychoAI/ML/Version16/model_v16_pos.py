from __future__ import absolute_import, division, print_function, unicode_literals
import os

from tensorflow.python.keras.layers import Input, Dense
from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences

from SQL_Wrapper import sql
import numpy as np


# Note: our Ai is named Mia
class Pos_Mia:
    __model = None
    __max_words = 94
    word2id = None
    id2label = {0: "happiness",
                1: "surprise",
                2: "love"}

    def __init__(self):
        """
            load our saved model and save it to model var.
            :param: None.
            :return: None
        """

        # load the wordIndex dict from the database
        self.__load_wordIndex()

        # load the model
        file_name = os.path.dirname(__file__) + '\\miaV16Pos.model'
        self.__model = load_model(file_name)

    def __load_wordIndex(self):
        """
            load the wordIndex from the database and save it to self.wordIndex: dict
            :param: None
            :return: None
        """
        data = sql.SELECT("SELECT * FROM word_index_tbl")

        self.word2id = dict()
        for e in data:
            self.word2id[e[1]] = e[0]

    def encode(self, sequence):
        """
            convert a sequence of words to numbers depending on the wordIndex
            :param sequence: vector of words/str [,,,]
            :return: vector of numbers/int
        """
        # print("encode func: ")
        # print(sequence)

        arr = list()
        for word in sequence:
            try:
                arr.append(self.word2id[word.replace("'", "")])
            except:
                arr.append(0)
        # print("encoded")
        # print(arr)
        return arr

    def predict(self, text):
        """
            take the user's problem as an input, convert it to relevant vector,
            return the predicted sentiment.
            :param text: string
            :return: sentiment: text
        """

        # print(text)
        # print(text.split(" "))
        # print(type(text))

        # string to vector of numbers by wordIndex
        if type(text) is not list:
            vec = self.encode(text.split(" "))
        else:
            vec = self.encode(text)

        # print('this is vec ->')
        # print(vec)

        v2 = list()
        v2.append(vec)

        # pad to the expected input length
        vec = pad_sequences(v2, self.__max_words, padding='post')

        # print('this is v2 ->')
        # print(vec)
        # print('type of vec -> ' + str(type(vec)))

        # get the sentiment id from the model
        label = self.__model.predict(vec)

        # print('this is label for the pos net ->')
        # print(label)

        # turn the sentiment id into text
        # label = self.id2label[0][label]

        # print(type(label))

        # print('this is label2 ->')
        # print(label)

        # print('index')
        index = np.where(label == np.amax(label))
        # print(index)

        # print('index lvl 1')
        # # print(index[0])
        # print(index[1])

        # print('index lvl 2')
        # # print(index[0][0])
        # print(index[1][0])

        # print('label is ->')
        # print(self.id2label[index[1][0]])
        # # print(self.id2label[index[0][0]])

        _sentiment = self.id2label[index[1][0]]

        # if _sentiment == 'good':
        #     pass
        return _sentiment

    def train(self, text, y_labels):
        """
            in case that our predicted sentiment is wrong... this func' gives Mia the x_labels and y_labels
            to train to get more accurate.
            :param x_labels: input for neural network: list of strings. [[], [], []] (1*Len)
            :param y_labels: the output of the neural network: list of the classes ["","",""]
            :return: None
        """

        # string to vector of numbers by wordIndex
        # x_labels = [self.encode(seq) for seq in x_labels]

        # pad to the expected input length
        # x_labels = [pad_sequences(seq, self.__max_words) for seq in x_labels]

        # vec = self.encode(text.split())
        # v2 = list()
        # v2.append(vec)
        # x_labels = pad_sequences(v2, self.__max_words, padding='post')

        # x_labels = [[self.word2id[word.replace("'", "")] for word in sentence] for sentence in text]

        x_labels = [self.encode(sentence) for sentence in text]

        # y_labels = [self.get_key(label, self.id2label) for label in y_labels]
        print(y_labels)
        target = y_labels[0]

        x_labels = pad_sequences(x_labels, self.__max_words)

        # Convert Y to numpy array
        y_labels = to_categorical(y_labels, num_classes=len(self.id2label), dtype='float32')

        while self.predict(text[0]) != self.id2label[target]:
            print("fitting...")
            self.__model.fit(x_labels, y_labels, epochs=1, batch_size=1, shuffle=True, verbose=1)

        # self.model.fit(self.X, self.Y, epochs=3, batch_size=64, validation_split=0.1, shuffle=True, verbose=1)


        self.__model.save('mia_v1.6.model')
        print("saved")
        self.__model = load_model('mia_v1.6.model')
        print("loaded")

    def get_key(self, val, dict):
        for key, value in dict.items():
            if val == value:
                return key
        return 0


def guess(__str):
    _mia = Pos_Mia()
    return _mia.predict(__str.replace("'", ""))


def __train(_input, _output):
    _mia = Pos_Mia()

    _input = [[word.replace("'", "") for word in sentence] for sentence in _input]

    _mia.train(_input, _output)
    return _mia


def manual_testing():
    _mia = Pos_Mia()
    while True:
        text = input("pos 1.6v enter input: ")
        print("your input is:" + text + ".")
        print(_mia.predict(text))
        _input = input("good? ")
        if _input == "no":
            print(_mia.id2label)
            label = input("what the label should be? ")
            print("your label is:" + label + ": " + _mia.id2label[int(label)] + ".")# + ":" + _mia.get_key(label, _mia.id2label).__str__() + ".")
            _mia = __train([text.split(' ')], [int(label)])
        elif _input == 'yes':
            print("that's nice")


if __name__ == "__main__":
    manual_testing()
