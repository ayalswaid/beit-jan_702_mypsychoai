import pandas as pd

dataset = pd.read_csv("emotion.data")

# input sentences is the X to train the network
input_sentences = dataset["text"].values.tolist()

# labels is the Y to the X
labels = dataset["emotions"].values.tolist()

infile = open('pos_dataset', 'w')
infile.write("text,emotion\n")

pos_emotions = ["happiness", "love", "surprise"]

positive = list()

for i, label in enumerate(labels, 0):
    if label in pos_emotions:
        print(label + " " + input_sentences[i] + " " + i.__str__())
        infile.write(input_sentences[i] + "," + label + "\n")