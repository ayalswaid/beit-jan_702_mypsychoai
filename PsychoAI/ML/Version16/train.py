import pandas as pd
import keras
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
import os
from SQL_Wrapper import sql

class Model:
    def __init__(self):
        self.DATA_PATH = "emotion.data"

        # load the data from our dataset
        self.__load_data()

        # edit and fix some words in the data .. more details in the func's doc
        self.__fix_data()

        # initiate the word_index and label_index
        self.__tokenizers_inits()

        # encode input sequences to matrix of integers and labels to one-hot code matrix
        self.__preprocessing()

        # define the model and its layers and compile it
        self.__create_model()


    def __load_data(self):
        """
        load the dataset as dataframe and store the input_sequences and labels in the class.
        :return: None
        """
        dataset = pd.read_csv(self.DATA_PATH)

        self.input_sequences = dataset["text"].values.tolist()
        self.labels = dataset["emotions"].values.tolist()
        self.max_words = max([len(x) for x in self.input_sequences])  # max words in a sequence
        print("max:", self.max_words)

    def __fix_data(self):
        """
        fix the data like: it s --> it's --> it is
        :return:
        """
        for i in self.input_sequences:
            if len(i) == 830:
                i = i.split()
                for j in i:
                    print(j)
        for i in range(len(self.input_sequences)):
            self.input_sequences[i] = self.input_sequences[i].replace(" it s ", " it's ")
            self.input_sequences[i] = self.input_sequences[i].replace(" i m ", " i'm")
            self.input_sequences[i] = self.input_sequences[i].replace(" doesn t ", " doesn't ")
            self.input_sequences[i] = self.input_sequences[i].replace(" don t ", " don't ")
            self.input_sequences[i] = self.input_sequences[i].replace(" couldn t ", " couldn't ")
            self.input_sequences[i] = self.input_sequences[i].replace(" shouldn t ", " shouldn't ")
            self.input_sequences[i] = self.input_sequences[i].replace(" i ve ", " i've ")
            self.input_sequences[i] = self.input_sequences[i].replace(" i d ", " i'd ")
            self.input_sequences[i] = self.input_sequences[i].replace(" can t ", " can't ")

    def __tokenizers_inits(self):
        """
        make a dict(word_index) that stores the most 50k used words in our data.
        word_index is stored in word2id dict in the class.
        :return: None
        """

        # tokenizer is the unit that tokenize - encode and give an id to each word - the data and
        #   and stores the word_index and more useful data structures.
        tokenizer = Tokenizer()

        tokenizer.fit_on_texts(self.input_sequences)

        # save some useful data for later usage.
        self.total_words = len(tokenizer.word_index) + 1 # 75,302
        self.max_words = max([len(x) for x in self.input_sequences]) # max words in a sequence
        print("max:", self.max_words)
        # finding the most used words and store them in word2id dict
        most_words = dict(tokenizer.word_counts)

        most_words = dict(sorted(most_words.items(), key=lambda x: x[1], reverse=True))

        # take the most 50k used words and store them in word2id format
        word2id = {key: value for (key, value) in [x for x in most_words.items()][:50000]}# word2id


        # create new word-index and set special words
        self.word2id = {}
        self.word2id['<PAD>'] = 0# <PAD> for padding
        self.word2id['<UNK>'] = 1# <UNK> for unknown words

        i = 2
        for k in word2id.keys():
            self.word2id[k] = i
            i += 1

        self.total_words = 50002

        # insert word2id dict into database for later usage
        for k, v in self.word2id.items():
            sql.INSERT("insert into word_index_tbl(id, word) VALUES({}, '{}')".format(v, k),
            "word_index_tbl")

        # label_index initializing ...

        # self.label2id = {l: i for i, l in enumerate(set(self.labels))}# 6 labels
        self.label2id = {'sadness': 0,
                         'love': 1,
                         'anger': 2,
                         'surprise': 3,
                         'fear': 4,
                         'joy': 5}# 6 labels

    def __preprocessing(self):
        """
        encode the data sequences(inputs) to a valid inputs for neural network(numbers) by the
        word2id dict
        :return: None
        """
        #sequences ...
        # split every string into sequence of words
        self.input_sequences = [seq.split(" ") for seq in self.input_sequences]

        new_seqs = []
        # encoding
        for seq in self.input_sequences:
            new_seqs.append([self.word2id.get(word, 1) for word in seq])# 1 is for <UNK>

        self.input_sequences = new_seqs
        print("max:", self.max_words)
        # padding the sequence
        self.input_sequences = pad_sequences(self.input_sequences, self.max_words, padding='post')
        print(self.input_sequences[:3])
        # labels ...
        # encode labels - string to id
        self.labels = [self.label2id[label] for label in self.labels]

        # from numbers to one-hot encoding numpy matrix
        self.labels = keras.utils.to_categorical(self.labels, num_classes=len(self.label2id), dtype='float32')


    def __create_model(self):
        """
        create the lstm model, compile and save it to self.model
        :return:
        """

        embedding_dim = 100
        print("max:", self.max_words)
        # Define input tensor
        sequence_input = keras.Input(shape=(self.max_words,), dtype='int32')

        # Word embedding layer
        embedded_inputs = keras.layers.Embedding(len(self.word2id) + 1,
                                                 embedding_dim,
                                                 input_length=self.max_words)(sequence_input)

        # Apply dropout to prevent overfitting
        embedded_inputs = keras.layers.Dropout(0.2)(embedded_inputs)

        # Apply Bidirectional LSTM over embedded inputs
        lstm_outs = keras.layers.wrappers.Bidirectional(
            keras.layers.LSTM(embedding_dim, return_sequences=True)
        )(embedded_inputs)

        # Apply dropout to LSTM outputs to prevent overfitting
        lstm_outs = keras.layers.Dropout(0.2)(lstm_outs)

        # Attention Mechanism - Generate attention vectors
        attention_vector = keras.layers.TimeDistributed(keras.layers.Dense(1))(lstm_outs)
        attention_vector = keras.layers.Reshape((self.max_words,))(attention_vector)
        attention_vector = keras.layers.Activation('softmax', name='attention_vec')(attention_vector)
        attention_output = keras.layers.Dot(axes=1)([lstm_outs, attention_vector])

        # Last layer: fully connected with softmax activation
        fc = keras.layers.Dense(embedding_dim, activation='relu')(attention_output)
        output = keras.layers.Dense(len(self.label2id), activation='softmax')(fc)

        # Finally building model
        self.model = keras.Model(inputs=[sequence_input], outputs=output)
        self.model.compile(loss="categorical_crossentropy", metrics=["accuracy"], optimizer='adam')

        print(self.model.summary())

    def train(self):
        """
        train our model with checkpoints - make a save to the weights of the NN for each epoch
        :return: None
        """
        # checkpoint file path
        checkpoint_path = "cp-{epoch:04d}.ckpt"
        os.path.dirname(checkpoint_path)

        # create checkpoint callback
        cp_callback = keras.callbacks.ModelCheckpoint(checkpoint_path,
                                                      save_weights_only=True,
                                                      verbose=1)

        del self.max_words
        del self.label2id
        del self.word2id
        del self.total_words


        self.model.fit(self.input_sequences,
                       self.labels,
                       epochs=3,
                       batch_size=64,
                       validation_split=0.1,
                       shuffle=True,
                       verbose=1,
                       callbacks=[cp_callback])

        # self.model.save('miaPlus.model')


if __name__ == "__main__":
    model = Model()
    model.train()
