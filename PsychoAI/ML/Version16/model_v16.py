from __future__ import absolute_import, division, print_function, unicode_literals
import os

from tensorflow.python.keras.layers import Input, Dense
from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences

from SQL_Wrapper import sql
import numpy as np

import ML.Version16.model_v16_pos as pos_mod
import ML.Version16.model_v16_neg as neg_mod




# Note: our Ai is named Mia
class Sentemint_Mia:
    __model = None
    __max_words = 110
    word2id = None
    id2label = {0: "bad",
                1: "good"}

    def __init__(self):
        """
            load our saved model and save it to model var.
            :param: None.
            :return: None
        """

        # load the wordIndex dict from the database
        self.__load_wordIndex()

        # load the model
        file_name = os.path.dirname(__file__) + '\\miaV16.model'
        self.__model = load_model(file_name)

    def __load_wordIndex(self):
        """
            load the wordIndex from the database and save it to self.wordIndex: dict
            :param: None
            :return: None
        """
        data = sql.SELECT("SELECT * FROM word_index_tbl")

        self.word2id = dict()
        for e in data:
            self.word2id[e[1]] = e[0]

    def encode(self, sequence):
        """
            convert a sequence of words to numbers depending on the wordIndex
            :param sequence: vector of words/str [,,,]
            :return: vector of numbers/int
        """
        # print("encode func: ")
        # print(sequence)

        arr = list()
        for word in sequence:
            try:
                arr.append(self.word2id[word.replace("'", "")])
            except:
                arr.append(0)
        # print("encoded")
        # print(arr)
        return arr

    def predict(self, text):
        """
            take the user's problem as an input, convert it to relevant vector,
            return the predicted sentiment.
            :param text: string
            :return: sentiment: text
        """
        # string to vector of numbers by wordIndex
        if type(text) is not list:
            vec = self.encode(text.split(" "))
        else:
            vec = self.encode(text)

        v2 = list()
        v2.append(vec)

        # pad to the expected input length
        vec = pad_sequences(v2, self.__max_words, padding='post')

        # get the sentiment id from the model
        label = self.__model.predict(vec)

        index = np.where(label == np.amax(label))

        _sentiment = self.id2label[index[1][0]]
        return _sentiment
        if _sentiment == 'good':
            emotion = pos_mod.guess(text)
        else:
            emotion = neg_mod.guess(text)

        return emotion

    def train(self, text, y_labels):
        """
            in case that our predicted sentiment is wrong... this func' gives Mia the x_labels and y_labels
            to train to get more accurate.
            :param text: input for neural network: list of strings. [[], [], []] (1*Len)
            :param y_labels: the output of the neural network: list of the classes ["","",""]
            :return: None
        """

        # string to vector of numbers by wordIndex
        # x_labels = [self.encode(seq) for seq in x_labels]

        # pad to the expected input length
        # x_labels = [pad_sequences(seq, self.__max_words) for seq in x_labels]

        # vec = self.encode(text.split())
        # v2 = list()
        # v2.append(vec)
        # x_labels = pad_sequences(v2, self.__max_words, padding='post')

        # x_labels = [[self.word2id[word.replace("'", "")] for word in sentence] for sentence in text]

        # y_labels = [self.get_key(label, self.id2label) for label in y_labels]
        print(y_labels)

        self.__model.save('mia_v1.6.model')
        print("saved")
        self.__model = load_model('mia_v1.6.model')
        print("loaded")

    def get_key(self, val, dict):
        for key, value in dict.items():
            if val == value:
                return key
        return 0

def guess(__str):
    _mia = Sentemint_Mia()
    return _mia.predict(__str.replace("'", ""))


def __train(_input, _output):
    _mia = Sentemint_Mia()

    _input = [[word.replace("'", "") for word in sentence] for sentence in _input]

    _mia.train(_input, _output)
    return _mia


def manual_testing():
    _mia = Sentemint_Mia()
    while True:
        text = input("1.6v enter input: ")
        print("your input is: " + text + ".")
        print(_mia.predict(text))


if __name__ == "__main__":
    manual_testing()
