import pandas as pd
from SQL_Wrapper.sql import SELECT, INSERT
dataset1 = pd.read_csv("unigram_freq.csv")
dataset2 = pd.read_csv("emotion.data")

cursor = SELECT("SELECT * FROM word_index_tbl ORDER BY id ASC")

_dataset = list()
i = 0
for elem in dataset2["text"].values.tolist():
    # print(elem.split(' '))
    for word in elem.split(' '):
        if word not in _dataset:
            _dataset.append(word.replace("'", ""))

print("33.333%")

for word in dataset1["word"].values.tolist():
    if word not in _dataset:
        try:
            _dataset.append(word.replace("'", ""))
        except:
            print(word)
print("66.666%")

i = 1
for word in _dataset:
    INSERT("INSERT INTO word_index_tbl (id, word) VALUES ({0}, '{1}')".format(i, word), "word_index_tbl")
    i += 1

print("100%\ndone")





