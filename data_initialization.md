# data structures

ML folder model.py file:

    id2label : converts the returned value in the predict function from id(number) into word(emotion) {id: emotion}
    word2id: converts the words given from the user into matrix(1*(length of sentence)) of ids {word, id}
    __max_words: used to cut/add the sentence so it's length is the __max_word
    
    
    
ML folder train.py file:

    word2id: converts the words given to the train into matrix(1*(length of sentence)) of ids {word, id}
    label2id: used to convert the Y (output) in the model from words to id 
    
    X: encoded input data -> type: matrix (1Xlength) [[], [], [], []]
    Y: encoded output data -> type: array ["","","",""]
    
    train(sentence, label)
          word list of list, word                  [["","","",""]], ""
          str      , str
    