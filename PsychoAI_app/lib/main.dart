import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.black
      ),
      routes: {
        "/": (_) => WebviewScaffold(
          url: 'https://82.81.32.217:5001/',
          ignoreSSLErrors: true,
          appBar: AppBar(
            title: Text('PsychoAI'),
          ),
        )
      },
    );
  }
}